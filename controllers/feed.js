import fs from 'fs';
import path from 'path';

import { fileURLToPath } from 'url';

import { validationResult } from 'express-validator';
import Post from '../models/post.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const getPosts = async (req, res, next) => {
    try {
        const posts = await Post.find();
        if (!posts) {
            const error = new Error('No posts found');
            error.statuCode = 404;
            next(error);
        }
        res.status(200).json({ message: 'Fetched posts successfully', posts: posts });
    } catch (error) {
        next(error);
    }
};

const createPost = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Validation failed');
        error.statusCode = 422;
        throw error;
    }

    if (!req.file) {
        const error = new Error('No image provided');
        error.statusCode = 422;
        throw error;
    }

    const title = req.body.title;
    const content = req.body.content;
    const imageUrl = req.file.path;
    const newPost = new Post({
        title,
        content,
        imageUrl: imageUrl,
        creator: { name: 'Tim Borodin' },
    });
    await newPost.save((err, doc) => {
        if (err) {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            throw err;
            // next(err);
        }
    });

    res.status(201).json({
        msg: 'Post created successfully',
        post: {
            // _id: new Date().toISOString(),
            title,
            content,
            creator: { name: 'Tim Borodin' },
            createdAt: new Date()
        }
    })
};

const getPost = async (req, res, next) => {
    const postId = req.params.postId;

    try {
        const post = await Post.findById(postId);
        if (!post) {
            const error = new Error('Post not found');
            error.statuCode = 404;
            next(error);
        }
        res.status(200).json({ message: 'Post fetched', post: post });
    } catch (error) {
        // console.log('🚀 ~ file: feed.js ~ line 69 ~ getPost ~ error', error);
        // res.status(400).send(error);
        next(error);
    }
}

const updatePost = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Validation failed');
        error.statusCode = 422;
        throw error;
    }

    const postId = req.params.postId;
    const title = req.body.title;
    const content = req.body.content;
    let imageUrl = req.body.image;
    if (req.file) {
        imageUrl = req.file.path;
    }
    if (!imageUrl) {
        const error = new Error('No file picked');
        error.statuCode = 422;
        throw error;
    }

    try {
        const post = await Post.findById(postId);
        if (!post) {
            const error = new Error('Post not found');
            error.statuCode = 404;
            throw error;
        }
        if (imageUrl !== post.imageUrl) {
            clearImage(post.imageUrl);
        }
        post.title = title;
        post.imageUrl = imageUrl;
        post.content = content;
        await post.save();
        res.status(200).json({ message: 'Post updated', post: post });
    } catch (error) {
        next(error);
    }
}

const clearImage = (filePath) => {
    filePath = path.join(__dirname, '..', filePath);
    fs.unlink(filePath, err => console.log(err));
}

export {
    getPosts,
    createPost,
    getPost,
    updatePost
};