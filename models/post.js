import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const postSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    content: {
        type: String,
        trim: true,
        required: true
    },
    creator: {
        type: Object,
        required: true
    }
}, { timestamps: true });

const Post = mongoose.model('Post', postSchema);

export { Post as default }