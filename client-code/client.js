const getBtn = document.getElementById('get');
const postBtn = document.getElementById('post');

getBtn.addEventListener('click', () => {
    fetch('http://localhost:8080/feed/posts')
        .then(res => res.json())
        .then(data => console.log(data))
        .catch(err => console.log(err));
});

postBtn.addEventListener('click', () => {
    fetch('http://localhost:8080/feed/post', {
        method: 'POST',
        body: JSON.stringify({
            title: 'Codepen post',
            content: 'Create via Codepen'
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(data => console.log(data))
        .catch(err => console.log(err));
});